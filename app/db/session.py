from typing import Union

from sqlalchemy.ext.asyncio import create_async_engine, AsyncEngine
from sqlalchemy.orm import sessionmaker
from sqlmodel.ext.asyncio.session import AsyncSession

from app.settings import settings


class DBSession:
    _session: Union[AsyncSession, None] = None
    _engine: Union[AsyncEngine, None] = None

    def __getattr__(self, name):
        return getattr(self._session, name)

    async def init(self):
        self._engine = create_async_engine(
            settings.DATABASE_CONNECTION_STRING, echo=True, future=True
        )

        async_session = sessionmaker(self._engine, class_=AsyncSession)
        self._session = async_session()

    async def close(self):
        await self._session.close()


db_session = DBSession()
