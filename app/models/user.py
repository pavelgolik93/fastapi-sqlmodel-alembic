from typing import Optional

from pydantic import EmailStr
from sqlalchemy import Column, String
from sqlmodel import SQLModel, Field


class UserBase(SQLModel):
    email: EmailStr = Field(
        sa_column=Column("email", String, unique=True), nullable=False
    )
    first_name: str
    last_name: str


class UserLogin(SQLModel):
    email: EmailStr
    password: str


class User(UserBase, table=True):
    id: Optional[int] = Field(default=None, primary_key=True, index=True)
    password: str


class UserRead(UserBase):
    id: int


class UserCreate(UserBase):
    password: str
