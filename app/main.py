from fastapi import FastAPI

from .db.session import db_session
from .api.v1.api import api_v1_router


app = FastAPI()
app.include_router(api_v1_router, prefix="/api/v1")


@app.on_event("startup")
async def startup_event():
    await db_session.init()


@app.on_event("shutdown")
async def shutdown_event():
    await db_session.close()
