import datetime
from typing import Union

from fastapi import Depends, HTTPException
from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials
from jose import jwt, JWTError
from starlette import status
from starlette.requests import Request

from app.crud.user import crud_user
from app.models import User
from app.settings import settings


class JWTAuthorization(HTTPBearer):
    payload: Union[dict, None] = None

    def __init__(self, auto_error: bool = True):
        super(JWTAuthorization, self).__init__(auto_error=auto_error)

    async def __call__(self, request: Request):
        credentials: HTTPAuthorizationCredentials = await super(
            JWTAuthorization, self
        ).__call__(request)
        if not credentials:
            raise HTTPException(
                status_code=status.HTTP_403_FORBIDDEN,
                detail="Invalid authorization code.",
            )

        if not credentials.scheme == "Bearer":
            raise HTTPException(
                status_code=status.HTTP_403_FORBIDDEN,
                detail="Invalid authentication scheme.",
            )

        self.payload = self.decode_token(credentials.credentials)
        if not self.verify_payload():
            raise HTTPException(
                status_code=status.HTTP_403_FORBIDDEN,
                detail="Invalid token or expired token.",
            )
        return self

    @property
    def user_id_clam(self):
        return self.payload.get(settings.JWT_USER_ID_CLAIM)

    def decode_token(self, token: str) -> Union[dict, None]:
        try:
            payload = jwt.decode(
                token, settings.SECRET_KEY, algorithms=[settings.JWT_ALGORITHM]
            )
        except JWTError:
            payload = None
        return payload

    def verify_payload(self) -> bool:
        is_token_valid = False

        if self.payload is not None:
            if self.user_id_clam is not None:
                is_token_valid = True

        return is_token_valid


async def get_current_user(
    auth_class: JWTAuthorization = Depends(JWTAuthorization()),
) -> User:
    user_id_clam = auth_class.payload.get(settings.JWT_USER_ID_CLAIM)

    user = await crud_user.get_by_pk(user_id_clam)
    if user is None:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Invalid token or expired token.",
        )
    return user


def create_access_token(user: User):
    user_id_field = getattr(user, settings.JWT_USER_ID_FIELD)
    if user_id_field is None:
        raise Exception("User has not field {}".format(settings.JWT_USER_ID_FIELD))
    expire = datetime.datetime.utcnow() + settings.JWT_ACCESS_TOKEN_LIFETIME
    payload = {
        settings.JWT_USER_ID_CLAIM: user_id_field,
        "exp": expire,
        "iat": datetime.datetime.utcnow(),
    }

    encoded_jwt = jwt.encode(
        payload, settings.SECRET_KEY, algorithm=settings.JWT_ALGORITHM
    )
    return encoded_jwt
