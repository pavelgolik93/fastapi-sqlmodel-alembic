from fastapi import APIRouter

from .private.api import private_router
from .public.api import public_router


api_v1_router = APIRouter()
api_v1_router.include_router(public_router, prefix="/public")
api_v1_router.include_router(private_router, prefix="/private")
