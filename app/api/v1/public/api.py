from fastapi import APIRouter

from .endpoints import auth, user


public_router = APIRouter()

public_router.include_router(auth.router, prefix="/auth", tags=["authorization public"])
public_router.include_router(user.router, prefix="/user", tags=["user public"])
