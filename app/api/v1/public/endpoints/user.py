from fastapi import HTTPException, APIRouter
from starlette import status

from app.crud.user import crud_user
from app.models import UserRead, User, UserCreate


router = APIRouter()


@router.post(
    "/",
    status_code=201,
    response_model=UserRead,
    responses={
        400: {"model": User, "description": "The user with this email already exists."}
    },
)
async def create_user(*, user: UserCreate):
    user_exist = await crud_user.checking_email_availability(user.email)
    if user_exist:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="The user with this email already exists.",
        )
    return await crud_user.create(user)
