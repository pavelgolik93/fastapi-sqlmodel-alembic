from fastapi import APIRouter, HTTPException
from starlette import status

from app.crud.user import crud_user
from app.models import UserLogin
from app.utils.encryption import verify_password
from app.utils.jwt_security import create_access_token


router = APIRouter()

login_responses = {
    200: {
        "description": "Successful Response",
        "content": {
            "application/json": {
                "example": {
                    "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjMsImV4cCI6MTY0NjQ4NzkwOSwiaWF0IjoxNjQ2NDg3MzA5fQ.xSKm9PFUKZJRZsDgUGf49q2U9qKvm0afXhJ3MJoo7KI",
                    "token_type": "bearer",
                }
            }
        },
    },
}


@router.post("/login", responses=login_responses)
async def login(auth_data: UserLogin) -> dict:
    user = await crud_user.get_by_email(auth_data.email)
    if not user or not verify_password(auth_data.password, user.password):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )

    return {
        "access_token": create_access_token(user),
        "token_type": "bearer",
    }
