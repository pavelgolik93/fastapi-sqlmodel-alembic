from fastapi import APIRouter

from .endpoints import user


private_router = APIRouter()

private_router.include_router(user.router, prefix="/user", tags=["user private"])
