from fastapi import APIRouter, Depends

from app.models.user import UserRead
from app.utils.jwt_security import get_current_user

router = APIRouter()


@router.get("/", status_code=200, response_model=UserRead)
async def current_user_info(*, current_user: UserRead = Depends(get_current_user)):
    return current_user
