from app.crud.base import CRUDBase
from app.models import UserCreate, UserRead, User
from app.utils.encryption import get_password_hash


class CRUDUser(CRUDBase[User, UserCreate, UserRead]):
    async def get_by_email(self, email: str) -> User:
        return await self._get(User.email == email)

    async def checking_email_availability(self, email: str) -> bool:
        result = await self.get_by_email(email)
        return result is not None

    async def create(self, user: User) -> User:
        user.password = get_password_hash(user.password)
        return await super().create(user)


crud_user = CRUDUser(User)
