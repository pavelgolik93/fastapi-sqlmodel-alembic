from typing import TypeVar, Generic, Type, Any

from pydantic import BaseModel
from sqlmodel import SQLModel, select
from sqlmodel.ext.asyncio.session import AsyncSession

from app.db.session import db_session


ModelType = TypeVar("ModelType", bound=SQLModel)
CreateSchemaType = TypeVar("CreateSchemaType", bound=BaseModel)
UpdateSchemaType = TypeVar("UpdateSchemaType", bound=BaseModel)


class CRUDBase(Generic[ModelType, CreateSchemaType, UpdateSchemaType]):
    def __init__(self, model: Type[ModelType]):
        self.db: AsyncSession = db_session
        self.model = model

    async def get_by_pk(self, pk: Any) -> ModelType:
        return await self.db.get(self.model, pk)

    async def _get(self, *whereclause) -> ModelType:
        statement = select(self.model).where(*whereclause)
        result = await self.db.exec(statement)
        return result.first()

    # def get_multi(
    #     self, db: Session, *, skip: int = 0, limit: int = 5000
    # ) -> List[ModelType]:
    #     return (
    #         db.query(self.model).order_by(self.model.id).offset(skip).limit(limit).all()
    #     )
    #
    async def create(self, obj_in: CreateSchemaType) -> ModelType:
        new_obj = self.model.from_orm(obj_in)
        self.db.add(new_obj)
        await self.db.commit()
        await self.db.refresh(new_obj)
        return new_obj

    #
    # def update(
    #     self,
    #     db: Session,
    #     *,
    #     db_obj: ModelType,
    #     obj_in: Union[UpdateSchemaType, Dict[str, Any]]
    # ) -> ModelType:
    #     obj_data = jsonable_encoder(db_obj)
    #     if isinstance(obj_in, dict):
    #         update_data = obj_in
    #     else:
    #         update_data = obj_in.dict(exclude_unset=True)
    #     for field in obj_data:
    #         if field in update_data:
    #             setattr(db_obj, field, update_data[field])
    #     db.add(db_obj)
    #     db.commit()
    #     db.refresh(db_obj)
    #     return db_obj
    #
    # def remove(self, db: Session, *, id: int) -> ModelType:
    #     obj = db.query(self.model).get(id)
    #     db.delete(obj)
    #     db.commit()
    #     return obj
