import os
from datetime import timedelta
from typing import Optional

from pydantic import BaseSettings


class Settings(BaseSettings):
    SECRET_KEY: str = os.environ["SECRET_KEY"]

    DATABASE_CONNECTION_STRING: Optional[str] = os.environ["DATABASE_CONNECTION_STRING"]

    JWT_ALGORITHM: str = "HS256"
    JWT_USER_ID_FIELD: str = "id"
    JWT_USER_ID_CLAIM: str = "user_id"
    JWT_ACCESS_TOKEN_LIFETIME: timedelta = timedelta(minutes=10)
    JWT_REFRESH_TOKEN_LIFETIME: timedelta = timedelta(days=7)

    class Config:
        case_sensitive = True


settings = Settings()
