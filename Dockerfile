FROM python:3.9-slim

RUN apt-get -y update

WORKDIR /app
COPY . /app

RUN pip install --upgrade pip \
    && pip install --no-cache-dir -r /app/requirements.txt

ENV PYTHONPATH /app
ENV PYTHONUNBUFFERED 1

EXPOSE 80
